'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = runGulpTasks;

var _gulp = require('gulp');

var _gulp2 = _interopRequireDefault(_gulp);

var _gulpLoadPlugins = require('gulp-load-plugins');

var _gulpLoadPlugins2 = _interopRequireDefault(_gulpLoadPlugins);

var _browserSync = require('browser-sync');

var _browserSync2 = _interopRequireDefault(_browserSync);

var _rimraf = require('rimraf');

var _rimraf2 = _interopRequireDefault(_rimraf);

var _panini = require('panini');

var _panini2 = _interopRequireDefault(_panini);

var _yargs = require('yargs');

var _yargs2 = _interopRequireDefault(_yargs);

var _lazypipe = require('lazypipe');

var _lazypipe2 = _interopRequireDefault(_lazypipe);

var _inky = require('inky');

var _inky2 = _interopRequireDefault(_inky);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _siphonMediaQuery = require('siphon-media-query');

var _siphonMediaQuery2 = _interopRequireDefault(_siphonMediaQuery);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _mergeStream = require('merge-stream');

var _mergeStream2 = _interopRequireDefault(_mergeStream);

var _beepbeep = require('beepbeep');

var _beepbeep2 = _interopRequireDefault(_beepbeep);

var _colors = require('colors');

var _colors2 = _interopRequireDefault(_colors);

var _gulpFile = require('gulp-file');

var _gulpFile2 = _interopRequireDefault(_gulpFile);

var _gulpUtil = require('gulp-util');

var _gulpUtil2 = _interopRequireDefault(_gulpUtil);

var _vinylContentsTostring = require('vinyl-contents-tostring');

var _vinylContentsTostring2 = _interopRequireDefault(_vinylContentsTostring);

var _mapStream = require('map-stream');

var _mapStream2 = _interopRequireDefault(_mapStream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = (0, _gulpLoadPlugins2.default)();

// Look for the --production flag
var PRODUCTION = !!_yargs2.default.argv.production;

// Declar var so that both AWS and Litmus task can use it.
var CONFIG;

function runGulpTasks(pseudoFileName, inputHtml, sendAsResponse) {
  var css = _fs2.default.readFileSync('src/css/app.css').toString();
  var mqCss = (0, _siphonMediaQuery2.default)(css);

  (0, _gulpFile2.default)(pseudoFileName, inputHtml, { src: true }).pipe((0, _panini2.default)({
    root: 'src/pages',
    layouts: 'src/layouts',
    partials: 'src/partials',
    helpers: 'src/helpers'
  })).pipe((0, _inky2.default)()).pipe(inliner('src/css/app.css')).pipe((0, _mapStream2.default)(function (file, cb) {
    return (0, _vinylContentsTostring2.default)(file).then(function (contents) {
      sendAsResponse(contents);
    }).asCallback(cb);
  }));
}

// Build the "dist" folder by running all of the above tasks
_gulp2.default.task('build', _gulp2.default.series(clean, pages, sass, images, inline));

// Build emails, run the server, and watch for file changes
_gulp2.default.task('default', _gulp2.default.series('build', server, watch));

// Build emails, then send to litmus
_gulp2.default.task('litmus', _gulp2.default.series('build', creds, aws, litmus));

// Build emails, then zip
_gulp2.default.task('zip', _gulp2.default.series('build', zip));

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
  (0, _rimraf2.default)('dist', done);
}

// Compile layouts, pages, and partials into flat HTML files
// Then parse using Inky templates
function pages() {
  return _gulp2.default.src('src/pages/**/*.html').pipe((0, _panini2.default)({
    root: 'src/pages',
    layouts: 'src/layouts',
    partials: 'src/partials',
    helpers: 'src/helpers'
  })).pipe((0, _inky2.default)()).pipe(_gulp2.default.dest('dist'));
}

// Reset Panini's cache of layouts and partials
function resetPages(done) {
  _panini2.default.refresh();
  done();
}

// Compile Sass into CSS
function sass() {
  return _gulp2.default.src('src/assets/scss/app.scss').pipe($.if(!PRODUCTION, $.sourcemaps.init())).pipe($.sass({
    includePaths: ['node_modules/foundation-emails/scss']
  }).on('error', $.sass.logError)).pipe($.if(!PRODUCTION, $.sourcemaps.write())).pipe(_gulp2.default.dest('dist/css'));
}

// Copy and compress images
function images() {
  return _gulp2.default.src('src/assets/img/**/*').pipe($.imagemin()).pipe(_gulp2.default.dest('./dist/assets/img'));
}

// Inline CSS and minify HTML
function inline() {
  return _gulp2.default.src('dist/**/*.html').pipe($.if(PRODUCTION, inliner('dist/css/app.css'))).pipe(_gulp2.default.dest('dist'));
}

// Start a server with LiveReload to preview the site in
function server(done) {
  _browserSync2.default.init({
    server: 'dist'
  });
  done();
}

// Watch for file changes
function watch() {
  _gulp2.default.watch('src/pages/**/*.html').on('change', _gulp2.default.series(pages, inline, _browserSync2.default.reload));
  _gulp2.default.watch(['src/layouts/**/*', 'src/partials/**/*']).on('change', _gulp2.default.series(resetPages, pages, inline, _browserSync2.default.reload));
  _gulp2.default.watch(['../scss/**/*.scss', 'src/assets/scss/**/*.scss']).on('change', _gulp2.default.series(resetPages, sass, pages, inline, _browserSync2.default.reload));
  _gulp2.default.watch('src/assets/img/**/*').on('change', _gulp2.default.series(images, _browserSync2.default.reload));
}

// Inlines CSS into HTML, adds media query CSS into the <style> tag of the email, and compresses the HTML
function inliner(css) {
  var css = _fs2.default.readFileSync(css).toString();
  var mqCss = (0, _siphonMediaQuery2.default)(css);

  var pipe = (0, _lazypipe2.default)().pipe($.inlineCss, {
    applyStyleTags: false,
    removeStyleTags: false,
    removeLinkTags: false
  }).pipe($.replace, '<!-- <style> -->', '<style>' + mqCss + '</style>').pipe($.htmlmin, {
    collapseWhitespace: true,
    minifyCSS: true
  });

  return pipe();
}

// Ensure creds for Litmus are at least there.
function creds(done) {
  var configPath = './config.json';
  try {
    CONFIG = JSON.parse(_fs2.default.readFileSync(configPath));
  } catch (e) {
    (0, _beepbeep2.default)();
    console.log('[AWS]'.bold.red + ' Sorry, there was an issue locating your config.json. Please see README.md');
    process.exit();
  }
  done();
}

// Post images to AWS S3 so they are accessible to Litmus test
function aws() {
  var publisher = !!CONFIG.aws ? $.awspublish.create(CONFIG.aws) : $.awspublish.create();
  var headers = {
    'Cache-Control': 'max-age=315360000, no-transform, public'
  };

  return _gulp2.default.src('./dist/assets/img/*')
  // publisher will add Content-Length, Content-Type and headers specified above
  // If not specified it will set x-amz-acl to public-read by default
  .pipe(publisher.publish(headers))

  // create a cache file to speed up consecutive uploads
  //.pipe(publisher.cache())

  // print upload updates to console
  .pipe($.awspublish.reporter());
}

// Send email to Litmus for testing. If no AWS creds then do not replace img urls.
function litmus() {
  var awsURL = !!CONFIG && !!CONFIG.aws && !!CONFIG.aws.url ? CONFIG.aws.url : false;

  return _gulp2.default.src('dist/**/*.html').pipe($.if(!!awsURL, $.replace(/=('|")(\/?assets\/img)/g, "=$1" + awsURL))).pipe($.litmus(CONFIG.litmus)).pipe(_gulp2.default.dest('dist'));
}

// Copy and compress into Zip
function zip() {
  var dist = 'dist';
  var ext = '.html';

  function getHtmlFiles(dir) {
    return _fs2.default.readdirSync(dir).filter(function (file) {
      var fileExt = _path2.default.join(dir, file);
      var isHtml = _path2.default.extname(fileExt) == ext;
      return _fs2.default.statSync(fileExt).isFile() && isHtml;
    });
  }

  var htmlFiles = getHtmlFiles(dist);

  var moveTasks = htmlFiles.map(function (file) {
    var sourcePath = _path2.default.join(dist, file);
    var fileName = _path2.default.basename(sourcePath, ext);

    var moveHTML = _gulp2.default.src(sourcePath).pipe($.rename(function (path) {
      path.dirname = fileName;
      return path;
    }));

    var moveImages = _gulp2.default.src(sourcePath).pipe($.htmlSrc({ selector: 'img' })).pipe($.rename(function (path) {
      path.dirname = fileName + '/assets/img';
      return path;
    }));

    return (0, _mergeStream2.default)(moveHTML, moveImages).pipe($.zip(fileName + '.zip')).pipe(_gulp2.default.dest('dist'));
  });

  return (0, _mergeStream2.default)(moveTasks);
}
