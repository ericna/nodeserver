var express = require('express');
var app = express();
var bodyParser = require('body-parser');

import runGulpTasks from './gulpfile.js';
const PORT = 8080;
const HOST = '0.0.0.0';

app.use(bodyParser.json());

app.post('/compileFoundationEmail', function (req, res) {
  if (!req.body) return res.sendStatus(400);
  var emailHtml = req.body.emailHtml;

  runGulpTasks('src/test.html', emailHtml, function(compiledEmailHtml) {
    res.json({compiledEmailHtml: compiledEmailHtml});
  });
});

app.listen(PORT, function () {
  console.log('Example app listening on port %s', PORT);
});
