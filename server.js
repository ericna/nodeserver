'use strict';

var _gulpfile = require('./gulpfile.js');

var _gulpfile2 = _interopRequireDefault(_gulpfile);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var PORT = 8080;
var HOST = '0.0.0.0';

app.use(bodyParser.json());

app.post('/compileFoundationEmail', function (req, res) {
  if (!req.body) return res.sendStatus(400);
  var emailHtml = req.body.emailHtml;

  (0, _gulpfile2.default)('src/test.html', emailHtml, function (compiledEmailHtml) {
    res.json({ compiledEmailHtml: compiledEmailHtml });
  });
});

app.listen(PORT, function () {
  console.log('Example app listening on port %s', PORT);
});
